<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table ='movies';
    protected $filliable =['movies','description'];
    protected $guarded ='id';
}
