<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('movie', 'MovieController');
Route::post('movie/show', ['as'=>'movie/show', 'uses' => 'MovieController@show']);
Route::get('movie/update{id}', ['as'=>'movie/update', 'uses' => 'MovieController@update']);
